<!DOCTYPE html>
<html lang="en">
<head>

    <?php
        include 'includes/header.php'
    ?>

</head>
<body>

    <?php
        include 'includes/navigation.php'
    ?>

    <div class="container">

        <?php

            // SET SEARCH_PATH
            $pdo->exec("SET SEARCH_PATH TO immo");

            // Une phrase SQL

            $sql = "SELECT * FROM clients
                    INNER JOIN villes ON clients.codeville = villes.codeville
                    ORDER BY nomclient ASC;";

            // Créer une requête
            $requete = $pdo->prepare($sql);
            $requete->execute();

            // Récupérer les données de la requête
            $donnees = $requete->fetchAll();
            $total = count($donnees);

        ?>

        <h2 class="mt-3">Liste des clients <span class="bg-info text-white btn-lg"><i class="bi bi-person-circle"></i> <?php echo $total; ?></span></h2>

            <table class="table table-striped">
                <thead class="text-center">
                    <tr>
                        <th>Nom</th>
                        <th>Prénom</th>
                        <th>Titre</th>
                        <th>Code postal</th>
                        <th>Ville</th>
                    </tr>
                </thead>
                <tbody  class="text-center">
                    <?php foreach($donnees as $ligne) : ?>
                        <tr>
                                <td><?= $ligne['nomclient'] ?></td>
                                <td><?= $ligne['prenomclient'] ?></td>
                                <td><?= $ligne['titre'] ?></td>
                                <td><?= $ligne['codepostal'] ?></td>
                                <td><?= $ligne['nomville'] ?></td>
                        </tr>
                    <?php endforeach ?>
                </tbody>
            </table>

    </div>

    <?php
        include 'includes/footer.php'
    ?>

</body>
</html>