<!DOCTYPE html>
<html lang="en">
<head>

    <?php
        include 'includes/header.php'
    ?>

</head>
<body>

    <?php
        include 'includes/navigation.php'
    ?>

    <div class="container">

        <?php

            $id = $_GET['id'];

            // SET SEARCH_PATH
            $pdo->exec("SET SEARCH_PATH TO immo");

            // Une phrase SQL

            $sql = "SELECT nomproprietaire, prenomproprietaire, telephonepersonnel, telephonemobile, email
                    FROM proprietaires
                    WHERE numeroproprietaire = $id;";

            // Créer une requête
            $requete = $pdo->prepare($sql);
            $requete->execute();

            // Récupérer les données de la requête
            $donnees = $requete->fetch();

        ?>

        <div class="row">

            <div class="col-lg-4 mt-3">
                <h2 class="mt-3"><i class="bi bi-person-circle"></i> Propriétaire</h2>
                <div class="bg-info text-white btn-lg mt-3" style="width: 100%"><i class="bi bi-person-circle"></i> <?= $donnees['nomproprietaire'] ?> <?= $donnees['prenomproprietaire'] ?></div>
                <table class="table mt-2">
                    <tbody>
                        <tr>
                            <td>Nom </td>
                            <td><strong><?= $donnees['nomproprietaire'] ?></strong></td>
                        </tr>
                        <tr>
                            <td>Prenom </td>
                            <td><strong><?= $donnees['prenomproprietaire'] ?></strong></td>
                        </tr>
                        <tr>
                            <td>Téléphone personnel </td>
                            <td><?= $donnees['telephonepersonnel'] ?></td>
                        </tr>
                        <tr>
                            <td>Téléphone mobile </td>
                            <td><?= $donnees['telephonemobile'] ?></td>
                        </tr>
                        <tr>
                            <td>Email </td>
                            <td><?= $donnees['email'] ?></td>
                        </tr>
                    </tbody>
                </table>
            </div>

            <?php

                $id = $_GET['id'];

                // SET SEARCH_PATH
                $pdo->exec("SET SEARCH_PATH TO immo");

                // Une phrase SQL

                $sql = "SELECT adresse1, codepostal, nomville, intituletransaction, intitulebien, biens.pieces, biens.montant
                        FROM biens
                            INNER JOIN villes ON biens.codeville = villes.codeville
                            INNER JOIN typestransactions ON biens.codetransaction = typestransactions.codetransaction
                            INNER JOIN typesbiens ON biens.codebien = typesbiens.codebien
                        WHERE numeroproprietaire = $id
                        ORDER BY montant DESC;";

                // Créer une requête
                $requete = $pdo->prepare($sql);
                $requete->execute();

                // Récupérer les données de la requête
                $donnees = $requete->fetchAll();
                $total = count($donnees);

            ?>

            <div class="col-lg-8 mt-3">

                <h2 class="mt-3">Liste des biens <span class="bg-info text-white btn-lg"><i class="bi bi-person-circle"></i> <?php echo $total; ?></span></h2>

                    <table class="table table-striped">
                        <thead class="text-center">
                            <tr>
                                <th>Adresse</th>
                                <th>Code postal</th>
                                <th>Villes</th>
                                <th>Transaction</th>
                                <th>Type de bien</th>
                                <th>Pièces</th>
                                <th>Montant</th>
                            </tr>
                        </thead>
                        <tbody class="text-center">
                            <?php foreach($donnees as $ligne) : ?>
                                <tr>
                                    <td><?= $ligne['adresse1'] ?></td>
                                    <td><?= $ligne['codepostal'] ?></td>
                                    <td><?= $ligne['nomville'] ?></td>
                                    <td><?= $ligne['intituletransaction'] ?></td>
                                    <td><?= $ligne['intitulebien'] ?></td>
                                    <?php if ($ligne['pieces'] >= 3) : ?>
                                        <td><span class="badge bg-success text-white"><?= $ligne['pieces'] ?></span></td>
                                    <?php else : ?>
                                        <td><span class="badge bg-secondary text-white"><?= $ligne['pieces'] ?></span></td>
                                    <?php endif ?>
                                    <?php if ($ligne['montant'] > 300000) : ?>
                                        <td><strong><?= $ligne['montant'] ?></strong></td>
                                    <?php else : ?>
                                        <td><?= $ligne['montant'] ?></td>
                                    <?php endif ?>
                                </tr>
                            <?php endforeach ?>
                        </tbody>
                    </table>
                <h6 class="mt-4 text-center">Copyright 2022 -Agence Immobilière Morbihannaise</h6>
        </div>
    </div>
</div>

    <?php
        include 'includes/footer.php'
    ?>

</body>
</html>