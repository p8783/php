<!DOCTYPE html>
<html lang="en">
<head>

    <?php
        include 'includes/header.php'
    ?>

</head>
<body>

    <?php
        include 'includes/navigation.php'
    ?>

    <div class="container">

        <div class="row mt-5">

            <div class="border p-4">
                <h4>Page en construction</h4>
                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
            </div>
            <h6 class="mt-4 text-center">Copyright 2022 -Agence Immobilière Morbihannaise</h6>

        </div>

    </div>

    <?php
        include 'includes/footer.php'
    ?>

</body>
</html>