<!DOCTYPE html>
<html lang="en">
<head>

    <?php
        include 'includes/header.php'
    ?>

</head>
<body>

    <?php
        include 'includes/navigation.php'
    ?>

    <div class="container">

        <div class="row mt-5">

            <div class="mt-2">
                <h4>Plan d'accès</h4>
                <div id="map-container-google-1" class="z-depth-1-half map-container">
                    <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2682.8529205958835!2d-3.361060084364654!3d47.7455083791941!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48105e5abd7550b7%3A0xebea35eb85af4f6a!2s19%20Quai%20des%20Indes%2C%2056100%20Lorient%2C%20France!5e0!3m2!1sfr!2sus!4v1649147046174!5m2!1sfr!2sus" width="100%" height="450" style="border:0;" allowfullscreen="" loading="lazy" referrerpolicy="no-referrer-when-downgrade"></iframe>
                </div>
            </div>
            <h6 class="mt-4 text-center">Copyright 2022 -Agence Immobilière Morbihannaise</h6>

        </div>

    </div>

    <?php
        include 'includes/footer.php'
    ?>

</body>
</html>